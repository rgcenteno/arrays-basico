/*
 * IES Pazo da Mercé
 * Programación 1º DAW
 */
package arraysbasicos;

import java.util.Scanner;

/**
 *
 * @author profesor
 */
public class Ejercicio6 {
    
    public static void erastotenesProgram(){
        Scanner teclado = new Scanner(System.in);        
        int tope = -1;
        do{
            System.out.println("Por favor inserte el valor máximo");
            boolean correcto = teclado.hasNextInt();            
            if(correcto){
                tope = teclado.nextInt();
            }
            else{
                System.out.println("Inserte un número!");
                teclado.nextLine();
            }
        }while(tope == -1);
        int[] resultado = cribaErastotenes(tope);
        System.out.println(mostrarPrimos(resultado));
    }
    
    private static int[] cribaErastotenes(int tope){
        int[] primos = createArray(tope);
        for(int i = 2; i*i <= tope; i++){
            for(int j = i; i*j <= tope; j++){
                int noPrimo = i * j;
                primos[noPrimo - 2] = -1;
            }
        }
        return primos;
    }
    
    private static int[] createArray(int tope){
        int[] res = new int[tope - 1];
        for(int i = 0; i < res.length; i++){
            res[i] = i + 2;
        }
        return res;
    }
    
    private static String mostrarPrimos(int[] primos){
        StringBuilder sb = new StringBuilder("[");
        for(int primo : primos){
            if(primo != -1){
                sb.append(primo).append(" ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
