/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraysbasicos;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio1_3 {
    
    public static void ordenarArrayProgram(){
        int[] numeros = { 16, 3, 2, 13, 5, 10, 11, 8, 9, 6, 7, 12, 4, 15, 14, 1};
        System.out.println(showArray(numeros));
        System.out.println(showArray(ordenarArray(numeros)));
        
    }
    
    private static int[] ordenarArray(int[] array){
        for(int i = 0; i< array.length - 1; i++){
            for(int j = i + 1; j < array.length; j++){
                if(array[j] < array[i]){
                    int auxiliar = array[i];
                    array[i] = array[j];
                    array[j] = auxiliar;
                }
            }
        }
        return array;
    }
    
    private static String showArray(int[] array){
        StringBuilder sb = new StringBuilder("[");
        for(int actual : array){
            sb.append(actual).append(" ");
        }
        sb.replace(sb.length() - 1, sb.length(), "]");
        return sb.toString();
    }
}
