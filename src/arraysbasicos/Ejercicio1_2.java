/*
 * IES Pazo da Mercé
 * Programación 1º DAW
 */
package arraysbasicos;

/**
 *
 * @author profesor
 */
public class Ejercicio1_2 {
    public static void invertirProgram(){
        int[] numeros = { 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010 };
        System.out.println(showArray(numeros));
        System.out.println(showArray(invertir(numeros)));
    }
    
    private static String showArray(int[] array){
        StringBuilder sb = new StringBuilder("[");
        for(int actual : array){
            sb.append(actual).append(" ");
        }
        sb.replace(sb.length() - 1, sb.length(), "]");
        return sb.toString();
    }
    
    private static int[] invertir(int[] array){
        int[] resultado = new int[array.length];
        for(int i = array.length - 1; i >= 0; i--){
            resultado[array.length - i - 1] = array[i];
        }
        return resultado;
    }
    
    private static int[] invertir2(int[] array){
        int[] resultado = new int[array.length];
        for(int i = 0; i < array.length; i++){
            resultado[i] = array[array.length - i - 1];
        }
        return resultado;
    }
}
