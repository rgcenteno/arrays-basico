/*
 * IES Pazo da Mercé
 * Programación 1º DAW
 */
package arraysbasicos;

import java.util.Scanner;

/**
 *
 * @author profesor
 */
public class Ejercicio1_1 {
    
    public static void notasProgram(){
        float[] notas = new float[10];
        Scanner teclado = new Scanner(System.in);
        for(int i = 0; i < notas.length; i++){
            boolean exito = false;
            do{
                System.out.println("Por favor inserte la nota " + (i + 1));
                if(teclado.hasNextFloat()){
                    float nota = teclado.nextFloat();
                    if(nota >= 0 && nota <= 10){
                        notas[i] = nota;
                        exito = true;
                    }
                    else{
                        System.out.println("La nota debe estar comprendida entre 0 y 10");
                    }
                }
                else{
                    System.out.println("Por favor inserte un número");
                }
                teclado.nextLine();
            }while(!exito);
        }
        //Aquí ya deberíamos tener el array rellenado
        System.out.println("Contenido del array: ");
        System.out.println(showArray(notas));
        System.out.printf("Valor mínimo: %f.\nValor máximo: %f.\nMedia: %f\n", getMinimo(notas), getMaximo(notas), getMedia(notas));
    }
    
    private static float getMinimo(float[] notas){
        float minimo = 10;
        for(float actual : notas){
            if(actual < minimo){
                minimo = actual;
            }
        }
        return minimo;
    }
    
    private static float getMaximo(float[] notas){
        float maximo = 0;
        for(float actual : notas){
            if(actual > maximo){
                maximo = actual;
            }
        }
        return maximo;
    }
    
    private static float getMedia(float[] notas){
        float sumatorio = 0;
        for(float actual: notas){
            sumatorio += actual;
        }
        return sumatorio / notas.length;
    }
    
    private static String showArray(float[] array){
        StringBuilder sb = new StringBuilder("[");
        for(float actual : array){
            sb.append(actual).append(" ");
        }
        sb.replace(sb.length() - 1, sb.length(), "]");
        return sb.toString();
    }
}
