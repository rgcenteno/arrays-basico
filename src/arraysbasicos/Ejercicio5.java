/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraysbasicos;

/**
 *
 * @author profesor
 */
public class Ejercicio5 {
            
    public static void bingoProgram(){
        final int MAX_BOLAS = 3;
        final int TAMANO_CARTON = 1;        
        int[] bolasSacadas = new int[MAX_BOLAS];
        for(int i = 0; i < bolasSacadas.length; i++){
            bolasSacadas[i] = -1;
        }
        int[] carton = generarCarton(MAX_BOLAS, TAMANO_CARTON);
        
        String entradaUsuario;
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        java.util.Random generador = new java.util.Random();
        int posicionBolasSacadas = 0;
        boolean esBingo = false;
        do{
            System.out.println("************************************");
            System.out.println("* 1. Sacar bola                    *");
            System.out.println("* 2. Salir del juego               *");
            System.out.println("************************************");            
            entradaUsuario = teclado.nextLine();
            if(entradaUsuario.equals("1")){
                int nuevaBola;
                do{
                    nuevaBola = generador.nextInt(MAX_BOLAS) + 1; 
                }while(existeNum(bolasSacadas, nuevaBola));
                bolasSacadas[posicionBolasSacadas] = nuevaBola;
                posicionBolasSacadas++;
                System.out.println("Carton:\n" + mostrarCarton(carton, bolasSacadas));
                System.out.println("Bolas sacadas:\n" + mostrarArray(bolasSacadas) + "\n");  
                esBingo = contenidoEn(carton, bolasSacadas);
                if(esBingo){
                    System.out.println("BINGO!!");
                }
            }
        }
        while(!entradaUsuario.equals("2") && !esBingo);
    }
    
    public static boolean existeNum(int[] sacadas, int bola){
        for(int actual: sacadas){
            if(actual == bola){
                return true;
            }
        }
        return false;
    }
    
    public static int[] generarCarton(int max, int tamano){
        java.util.Random generador = new java.util.Random();
        int[] carton = new int[tamano];
        for(int i = 0; i < tamano; i++){        
            int nuevoNumero;
            do{
                nuevoNumero = generador.nextInt(max) + 1;                
            }while(existeNum(carton, nuevoNumero));    
            carton[i] = nuevoNumero;
        }
        return carton;
    }
    
    public static String mostrarArray(int[] array){
        StringBuilder sb = new StringBuilder("[");
        for(int i = 0; i < array.length && array[i] != -1; i++){         
            sb.append(array[i]).append(" ");            
        }   
        sb.append("]");
        return sb.toString();
    }
    
    public static String mostrarCarton(int[] array, int[] bolas){
        StringBuilder sb = new StringBuilder("[");
        for(int i = 0; i < array.length; i++){  
            if(existeNum(bolas, array[i])){
                sb.append("*").append(array[i]).append("* ");            
            }
            else{
                sb.append(array[i]).append(" ");            
            }            
        }   
        sb.append("]");
        return sb.toString();
    }
    
    public static boolean contenidoEn(int[] carton, int[] bolas){
        for(int numCarton : carton){
            int pos = 0;
            /*
            Importante que primero comprobemos que pos es una posición válida porque si no podemos tener el problema de que accedamos a una posición que no exista
            */
            while(pos < bolas.length && bolas[pos] != numCarton){
                pos++;
            }
            if(pos >= bolas.length){
                return false;
            }
        }
        return true;
    }
}
