/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraysbasicos;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio1_6 {
    
    public static void ordenarMatrizProgram(){
        int[][] matriz =
        {

            {16, 3, 2, 13},

            {5, 10, 11, 8},

            {9, 6, 7, 12},

           {4, 15, 14, 1}

        };
        System.out.println("Versión foreach:");
        System.out.println(printMatrizForeach(matriz));
        System.out.println("Versión for:");
        System.out.println(printMatrizFor(matriz));
        System.out.println("Matriz ordenada: ");
        System.out.println(printMatrizFor(ordenarMatriz(matriz)));
    }
    
    public static String printMatrizForeach(int[][] matriz){
        StringBuilder sb = new StringBuilder("");
        for(int[] subarray : matriz){
            sb.append("|");
            for(int valor : subarray){
                sb.append(String.format("%3s", valor));                
            }
            sb.append("|\n");
        }
        return sb.toString();
    }
    
    public static String printMatrizFor(int[][] matriz){
        StringBuilder sb = new StringBuilder("");
        for(int i = 0; i < matriz.length; i++){
            sb.append("|");
            for(int j = 0; j < matriz[i].length; j++){
                sb.append(String.format("%3s", matriz[i][j]));
            }
            sb.append("|\n");
        }      
        return sb.toString();
    }
    
    public static int[][] ordenarMatriz(int[][] matriz){
        for(int i = 0; i < matriz.length; i++){
            for(int j = 0; j < matriz[i].length; j++){
                int valorX = j == (matriz[i].length - 1) ? i + 1 : i;
                for(int x = valorX; x < matriz.length; x++){
                    int valorY = (x == i) ? j + 1 : 0;
                    for(int y = valorY; y < matriz[x].length; y++){
                        if(matriz[i][j] > matriz[x][y]){
                            int auxiliar = matriz[i][j];
                            matriz[i][j] = matriz[x][y];
                            matriz[x][y] = auxiliar;
                        }
                    }                    
                }
            }
        }
        return matriz;
    }
}
