/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraysbasicos;

/**
 *
 * @author rgcenteno
 */
public class Anagrama {
    
    public static void anagramaProgram(){
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        System.out.println("Inserte la primera palabra");
        String s1 = teclado.nextLine();
        System.out.println("Inserte la segunda palabra");
        String s2 = teclado.nextLine();
        s1 = limpiarCadena(s1);
        s2 = limpiarCadena(s2);
        System.out.printf("Cadenas limpias \"%s\", \"%s\"\n", s1, s2);
        System.out.println("Es anagrama: " + esAnagrama(s1, s2));       
    }
    
    public static void esAnagramaV2Program(){
        java.util.Scanner teclado = new java.util.Scanner(System.in);
        System.out.println("Inserte la primera palabra");
        String s1 = teclado.nextLine();
        System.out.println("Inserte la segunda palabra");
        String s2 = teclado.nextLine();
        s1 = limpiarCadena(s1);
        s2 = limpiarCadena(s2);
        System.out.printf("Cadenas limpias \"%s\", \"%s\"\n", s1, s2);

        char[] s1Char = ordenarArray(s1.toCharArray());        
        char[] s2Char = ordenarArray(s2.toCharArray());        
        System.out.println(s1Char);
        System.out.println(s2Char);
        System.out.println("Es anagrama versión 2: " + compareArray(s1Char, s2Char));
    }
    
    public static String limpiarCadena(String s1){
        //Ponemos en minúsculas la String de entrada
        s1 = s1.toLowerCase();
        //Para ir almacenando los caracteres que son letras
        StringBuilder sb = new StringBuilder("");
        for(int i = 0; i < s1.length(); i++){
            char c = s1.charAt(i);
            if(Character.isLetter(c)){
                sb.append(c);                
            }
        }
        return sb.toString();
    }
    
    public static boolean esAnagrama(String s1, String s2){
        StringBuilder sb2 = new StringBuilder(s2);
        for(int i = 0; i < s1.length(); i++){
            String caracterABuscar = s1.substring(i, i + 1);
            int index = sb2.indexOf(caracterABuscar);
            if(index == -1){
                return false;
            }
            else{
                sb2.deleteCharAt(index);
            }
        }
        /*if(sb2.length() == 0){
            return true;
        }
        else{
            return false;
        }*/
        return sb2.length() == 0;
    }
    
    public static char[] ordenarArray(char[] charArray){
        for(int i = 0; i < charArray.length; i++){
            for(int j = i + 1; j < charArray.length; j++){
                if(charArray[i] > charArray[j]){
                    char aux = charArray[i];
                    charArray[i] = charArray[j];
                    charArray[j] = aux;
                }
            }
        }
        return charArray;
    }
            
    public static boolean compareArray(char[] s1Array, char[] s2Array){
        if(s1Array.length == s2Array.length){
            for(int i = 0; i < s1Array.length; i++){
                if(s1Array[i] != s2Array[i]){
                    return false;
                }
            }
            return true;
        }
        else{
            return false;
        }
    }
}
