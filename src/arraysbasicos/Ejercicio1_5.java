/*
 * IES Pazo da Mercé
 * Programación 1 DAW
 */
package arraysbasicos;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio1_5 {
    
    public static void euroMillonesProgram(){
        int[] numeros = generarAleatorio(5, 50);
        int[] estrellas = generarAleatorio(2, 10);
        System.out.println("Números:\n" + showArray(numeros));
        System.out.println("Estrellas:\n" + showArray(estrellas) + "\n");
    }
    
    private static int[] generarAleatorio(int tamano, int max){
        int[] resultado = new int[tamano];
        java.util.Random generador = new java.util.Random();
        for(int i = 0; i < resultado.length; i++){
            int numero;
            do{
                numero = (generador.nextInt(max) + 1);
            }while(estaRepetido(numero, resultado));
            resultado[i] = numero;
        }
        return ordenarArray(resultado);
    }
    
    private static boolean estaRepetido(int numero, int[] array){
        for(int actual : array){
            if(actual == numero){
                return true;
            }
        }
        return false;
    }
    
    private static int[] ordenarArray(int[] array){
        for(int i = 0; i < array.length; i++){
            for(int j = i + 1; j < array.length; j++){
                if(array[i] > array[j]){
                    int auxiliar = array[i];
                    array[i] = array[j];
                    array[j] = auxiliar;
                }
            }
        }
        return array;
    }
    
    private static String showArray(int[] array){
        StringBuilder sb = new StringBuilder("[");
        for(int actual : array){
            sb.append(actual).append(" ");
        }
        sb.replace(sb.length() - 1, sb.length(), "]");
        return sb.toString();
    }
}
