/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package arraysbasicos;

import java.util.Scanner;

/**
 *
 * @author profesor
 */
public class ArraysBasicos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String input;
        do{
            System.out.println("******************************");
            System.out.println("* 5. Bingo                   *");
            System.out.println("* 6. Criba de Erastotenes    *");
            System.out.println("* 11. Notas                  *");
            System.out.println("* 12. Invertir               *");
            System.out.println("* 13. Ordenar array          *");
            System.out.println("* 14. Generar nombre         *");
            System.out.println("* 15. Euromillones           *");
            System.out.println("* 16. Matriz                 *");
            System.out.println("* 17. Anagrama               *");
            System.out.println("*                            *");
            System.out.println("* 0. Salir                   *");
            System.out.println("******************************");
            input = teclado.nextLine();
            switch(input){
                case "5":
                    Ejercicio5.bingoProgram();
                    break;
                case "6":
                    Ejercicio6.erastotenesProgram();
                    break;
                case "11":
                    Ejercicio1_1.notasProgram();
                    break;
                case "12":
                    Ejercicio1_2.invertirProgram();
                    break;
                case "13":
                    Ejercicio1_3.ordenarArrayProgram();
                    break;
                case "14":
                    Ejercicio1_4.generarNombresProgram();
                    break;
                case "15":
                    Ejercicio1_5.euroMillonesProgram();
                    break;
                case "16":
                    Ejercicio1_6.ordenarMatrizProgram();
                    break;
                case "17":
                    Anagrama.esAnagramaV2Program();
                    break;
            }
        }while(!input.equals("0"));
    }
    
}
