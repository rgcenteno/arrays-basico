/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package arraysbasicos;

/**
 *
 * @author rgcenteno
 */
public class Ejercicio1_4 {
    static String [] nombre = {"Carlos", "Miguel", "Iván", "Benjamín", "Francisco", "Erik", "Alexis Jose", "Marcos", "Cristopher", "Mauricio", "Jose Simon", "Nuria Maria"};
    static String [] apellido1 = {"Alvarez", "Candeira", "Casas", "Dominguez", "Fernandez", "Ferreira", "Giraldez", "Gonzalez", "Juncal", "Montes", "Sanchez", "da Silva", "Suarez"};
    static String [] apellido2 = {"Sanchez", "Carrera", "Cerqueira", "Fernandez", "Araujo", "Oset", "Groba", "Pereira", "Abeledo", "Iglesias", "Gonzalez", "Vilas"};
    static int numero = 0;
    
    public static void generarNombresProgram(){
        for(int i = 0; i < 10; i++){
            System.out.println(generarNombreAleatorio());
        }
        System.out.println("------------------------");
        encontrarApellidosDuplicados();
    }
    
    public static String generarNombreAleatorio(){
        /*
        String [] nombre = {"Carlos", "Miguel", "Iván", "Benjamín", "Francisco", "Erik", "Alexis Jose", "Marcos", "Cristopher", "Mauricio", "Jose Simon", "Nuria Maria"};
        String [] apellido1 = {"Alvarez", "Candeira", "Casas", "Dominguez", "Fernandez", "Ferreira", "Giraldez", "Gonzalez", "Juncal", "Montes", "Sanchez", "da Silva", "Suarez"};
        String [] apellido2 = {"Sanchez", "Carrera", "Cerqueira", "Fernandez", "Araujo", "Oset", "Groba", "Pereira", "Abeledo", "Iglesias", "Gonzalez", "Vilas"};
        */
        java.util.Random generador = new java.util.Random();
        int posNombre = generador.nextInt(nombre.length);
        int posApellido = generador.nextInt(apellido1.length);
        int posApellido2 = generador.nextInt(apellido2.length);
        return nombre[posNombre] + " " + apellido1[posApellido] + " " + apellido2[posApellido2];
    }
    
    public static void encontrarApellidosDuplicados(){
        for(int i = 0; i < apellido1.length; i++){
            boolean encontrado = false;
            for(int j = 0; !encontrado && j < apellido2.length; j++){
                if(apellido1[i].equals(apellido2[j])){
                    System.out.println(apellido1[i]);
                    encontrado = true;
                }
            }
        }
    }
}
